package com.tianyalei.zuul.zuulauth;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GateApp {


    public static void main(String[] args) {
        SpringApplication.run(GateApp.class, args);
    }


}
